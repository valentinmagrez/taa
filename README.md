# Partie TAA #

1. ### Lancement du serveur ###

A la racine du projet lancer la commande:

```
#!shell
mvn sprint-boot:run

```

2. ### Accès à l'API ###

Les entités accessiblent via l'API vont être: "users", "teams" et "projects"

Les accès à l'API

* GET:  (Recupère tous les éléments de l'entité spécifiée)
```
#!shell
url -H "Content-Type: application/json" -X GET http://localhost:8080/{{entityName}}

```

* POST:  

```
#!shell

curl -H "Content-Type: application/json" -X POST -d '{parameters}' http://localhost:8080/{{entityName}}

```

* DELETE:

```
#!shell
curl -i -X DELETE http://localhost:8080/{{entityName}}/{{entityID}}

```

* UPDATE:

```
#!shell
curl -i -X PUT -H "Content-Type:application/json" http://localhost:8080/{{entityName}}/{{entityId}} -d '{newValues}'

```


# Partie GLI #

## ANGULARJS ##

1. ### Lancement du serveur ###

Se rendre dans le dossier à l'aide de la commande:

```
#!shell
cd src/main/webapp/

```

Lancer le serveur:

```
#!shell
grunt serve

```

Accéder au site à l'url: http://localhost:9000

Se rendre dans l'onglet "My workspace".

Il est possible de:

* consulter les différentes entités en cliquant sur les éléments centraux
* ajouter des éléments grâce aux bouttons "add"
* mettre à jour des éléments en cliquant sur ceux-ci
* supprimer des élément en cliquant dessus, et ensuite cliquer sur supprimer


## GWT ##

1. ### Lancement du serveur ###

Se rendre dans le dossier à l'aide de la commande:

```
#!shell
cd src/main/webapp/gwt

```

```
#!shell
mvn gwt:run

```

Au chargement de la page toutes les infos des entités sont chargés.
Il est aussi possible d'ajouter des éléments.
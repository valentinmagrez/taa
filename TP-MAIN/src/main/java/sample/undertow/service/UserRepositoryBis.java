package sample.undertow.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import sample.undertow.domain.User;

@Repository
public class UserRepositoryBis{
    EntityManagerFactory factory = Persistence.createEntityManagerFactory("mysql");
	private EntityManager manager = factory.createEntityManager();
    EntityTransaction tx = manager.getTransaction();
	
	public void setManager(EntityManager manager) {
		this.manager = manager;
	}
	
	public void create(User user){
		tx.begin();
		manager.persist(user);
		tx.commit();
	}
	
	public void delete(long id){
		User u=findById(id);
		tx.begin();
		if(u!=null){
			manager.remove(u);
		}
		tx.commit();
	}
	
	
	//Recupere un utilsiateur par son id
	public User findById(long id){
		
        tx.begin();
        Optional<User> user = Optional.empty();
        try {
            user = Optional.of(manager.createQuery("SELECT e from User e WHERE e.id = :id", User.class)
                    .setParameter("id", id)
                    .getSingleResult());

        } catch (Exception e) {
            
        }
        tx.commit();
        
        if(user.isPresent()){
        	return user.get();
        }
        return null;
	}
	
	
	//Recupere la liste de tout les utilisateurs
	public List<User> findAll(){
		return manager.createQuery("Select u from user u", User.class).getResultList();
	}
	
	
	//Recupere un utilisateur par son nom
	public List<User> getUserByName(String username){
        final CriteriaBuilder qb = manager.getCriteriaBuilder();
        final CriteriaQuery<User> cq = qb.createQuery(User.class);
        final Root<User> from = cq.from(User.class);

        cq.select(from);
        cq.where(qb.equal(from.get("username"), "test"));
        return manager.createQuery(cq).getResultList();
	}
	
	
	//Fonction test pour l'api
	public List<User>test(){
		User u = new User("test");
		ArrayList<User> ulist = new ArrayList<User>();
		ulist.add(u);
		return ulist;
	}
	
	public void update(User u){
		tx.begin();
		manager.merge(u);
		tx.commit();
	}
}

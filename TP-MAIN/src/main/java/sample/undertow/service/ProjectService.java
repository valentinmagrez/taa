package sample.undertow.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import sample.undertow.domain.Project;
import sample.undertow.domain.Team;
import sample.undertow.repository.ProjectRepository;


@Component
@Transactional
public class ProjectService {
	
	private final ProjectRepository projectRepository;
	
	@Autowired
	public ProjectService(ProjectRepository projectRepository){
		this.projectRepository = projectRepository;
	}
	
	//Recupère un projet par son id
	public Project findById(long id){
		return projectRepository.findOne(id);
	}
	
	//Recupère la liste de tous les projets
	public List<Project> findAll(){
		return (List<Project>) projectRepository.findAll();
	}
	
	public void create(Project p) {
		projectRepository.save(p);
	}
	
	public void delete(long id){
		projectRepository.delete(id);
	}	
	
	public void update(Project p){
		projectRepository.save(p);
	}

}

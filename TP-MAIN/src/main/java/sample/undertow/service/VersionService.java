package sample.undertow.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import sample.undertow.domain.Version;
import sample.undertow.repository.VersionRepository;

@Component
@Transactional
public class VersionService {
	private final VersionRepository versionRepository;
	
	@Autowired
	public VersionService(VersionRepository versionRepository) {
		this.versionRepository = versionRepository;
	}
	
	//Recupere un utilsiateur par son id
	public Version findById(long id){
		return versionRepository.findOne(id);
	}
	
	public List<Version> findAll(){
		return (List<Version>) versionRepository.findAll();
	}
	
	public void create(Version u){
		versionRepository.save(u);
	}
	
	public void delete(long id){
		versionRepository.delete(id);
	}
	
	public void update(Version u){
		versionRepository.save(u);
	}
}

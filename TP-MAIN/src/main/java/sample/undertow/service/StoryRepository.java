package sample.undertow.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.omg.PortableInterceptor.USER_EXCEPTION;
import org.springframework.stereotype.Component;

import sample.undertow.domain.Story;
import sample.undertow.domain.User;

@Component
public class StoryRepository {
    EntityManagerFactory factory = Persistence.createEntityManagerFactory("mysql");
    EntityManager manager = factory.createEntityManager();
	
	public void setManager(EntityManager manager) {
		this.manager = manager;
	}
	
	public void create(Story story){
		manager.persist(story);
	}
	
	public List<Story> findAll(){
		return manager.createQuery("Select u from Story u", Story.class).getResultList();
	}
}
package sample.undertow.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import sample.undertow.domain.User;
import sample.undertow.repository.UserRepository;

	@Component
	@Transactional
	public class UserService {

		private final UserRepository userRepository;
		
		@Autowired
		public UserService(UserRepository userRepository) {
			this.userRepository = userRepository;
		}
		
		//Recupere un utilsiateur par son id
		public User findById(long id){
			return userRepository.findOne(id);
		}
		
		public List<User> findAll(){
			return (List<User>) userRepository.findAll();
		}
		
		public void create(User u){
			userRepository.save(u);
		}
		
		public void delete(long id){
			userRepository.delete(id);
		}
		
		public void update(User u){
			userRepository.save(u);
		}
}

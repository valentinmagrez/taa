package sample.undertow.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import sample.undertow.domain.Team;
import sample.undertow.repository.TeamRepository;

@Component
@Transactional
public class TeamService {


		private final TeamRepository teamRepository;
		
		@Autowired
		public TeamService(TeamRepository teamRepository) {
			this.teamRepository = teamRepository;
		}
		
		//Recupere un utilisateur par son id
		public Team findById(long id){
			return teamRepository.findOne(id);
		}
		
		public List<Team> findAll(){
			return (List<Team>) teamRepository.findAll();
		}
		
		public void create(Team t){
			teamRepository.save(t);
		}
		
		public void delete(long id){
			teamRepository.delete(id);
		}
		
		public void update(Team t){
			System.out.println(t.getUsers().size());
			teamRepository.save(t);
		}
}
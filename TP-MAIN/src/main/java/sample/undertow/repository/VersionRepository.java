package sample.undertow.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import sample.undertow.domain.Version;

public interface VersionRepository extends JpaRepository<Version, Long>{

}

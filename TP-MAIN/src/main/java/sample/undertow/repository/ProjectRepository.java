package sample.undertow.repository;


import org.springframework.data.jpa.repository.JpaRepository;

import sample.undertow.domain.Project;


public interface ProjectRepository extends JpaRepository<Project, Long>{

	
}

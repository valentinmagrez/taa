package sample.undertow.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import sample.undertow.domain.Team;

public interface TeamRepository extends JpaRepository<Team, Long>{

}

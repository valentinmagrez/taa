package sample.undertow.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import sample.undertow.domain.User;

public interface UserRepository extends JpaRepository<User, Long> {


}

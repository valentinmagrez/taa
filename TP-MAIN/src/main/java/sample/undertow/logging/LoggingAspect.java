package sample.undertow.logging;

import java.util.Arrays;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class LoggingAspect {
	
	@Around("execution(public * sample.undertow.web.*.*(..))")
    public Object logAround(ProceedingJoinPoint joinPoint) throws Throwable {
            System.out.println("Enter: "+joinPoint.getSignature().getDeclaringTypeName()+"."+joinPoint.getSignature().getName()+" with argument[s] = "+Arrays.toString(joinPoint.getArgs()));
        try {
            Object result = joinPoint.proceed();
                System.out.println("Exit: "+joinPoint.getSignature().getDeclaringTypeName()+"."+joinPoint.getSignature().getName()+" with result = "+result);
            return result;
        } catch (IllegalArgumentException e) {
            System.out.println("Illegal argument: {"+Arrays.toString(joinPoint.getArgs())+"} in {"+joinPoint.getSignature().getDeclaringTypeName()+"}.{"+joinPoint.getSignature().getName()+"}()");

            throw e;
        }
    }
}

package sample.undertow.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import sample.undertow.domain.Priority;

@Entity(name="abstract_story")
public abstract class AbstractStory {

	@Id
	@GeneratedValue
	private long id;
	
	private int estimation;
	
	private Priority priority;
	
	private String description;
	
	@ManyToOne
	private Version version;

	public AbstractStory(int estimation, Priority priority, String description, Version version) {
		super();
		this.estimation = estimation;
		this.priority = priority;
		this.description = description;
		this.setVersion(version);
	}

	public AbstractStory() {
		super();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getEstimation() {
		return estimation;
	}

	public void setEstimation(int estimation) {
		this.estimation = estimation;
	}

	public Priority getPriority() {
		return priority;
	}

	public void setPriority(Priority priority) {
		this.priority = priority;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Version getVersion() {
		return version;
	}

	public void setVersion(Version version) {
		//If version was already define
		if(this.version!=version){
			System.out.println("version differente");
			if(this.version!=null){
				//Remove the old one
				this.version.getStories().remove(this);
				}
			if(!version.getStories().contains(this)){
				version.getStories().add(this);
			}
			this.version = version;
		}
	}

	@Override
	public String toString() {
		return "AbstractStory [id=" + id + ", estimation=" + estimation + ", priority=" + priority + ", description="
				+ description + ", version=" + version + "]";
	}
	
	
	
	
}

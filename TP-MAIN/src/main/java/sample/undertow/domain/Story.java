package sample.undertow.domain;
import sample.undertow.domain.Priority;
import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity(name="story")
public class Story extends AbstractStory{
	
	@OneToMany(mappedBy="story", cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
	private Collection<Task> tasks = new ArrayList<Task>();
	
	@ManyToOne
	private Epic epic;

	public Story(int estimation, Priority priority, String description, Version version, Epic epic) {
		super(estimation, priority, description, version);
		this.setEpic(epic);
		
		if(epic!=null && epic.getVersion()!=null){
			this.setVersion(epic.getVersion());
		}
	}
	
	public Story(){
		super();
	}

	public Collection<Task> getTasks() {
		return tasks;
	}

	public void setTasks(Collection<Task> tasks) {
		this.tasks = tasks;
	}

	public Epic getEpic() {
		return epic;
	}

	public void setEpic(Epic epic) {
		if(!epic.getStories().contains(this)){
			epic.getStories().add(this);
		}
		this.epic = epic;
	}
	
	public String toString(){
		String str = super.toString();
		
		if(!tasks.isEmpty()){
			for(Task task : tasks){
				str+= task+"\n";
			}
		}
		return str;
	}
	
}

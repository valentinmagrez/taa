package sample.undertow.domain;

public enum Priority {
	High,
	Medium,
	Low
}
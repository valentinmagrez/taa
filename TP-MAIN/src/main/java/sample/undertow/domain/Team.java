package sample.undertow.domain;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity(name="team")
public class Team {
	@Id
	@GeneratedValue
	private long id;
	
	@ManyToMany(cascade = CascadeType.MERGE)
	@JoinTable(name="users_teams",
			joinColumns = {@JoinColumn(name = "team_id", referencedColumnName = "id")},
			inverseJoinColumns = {@JoinColumn(name = "user_id", referencedColumnName = "id")}
	)
	@JsonIgnore
	private Set<User> users = new HashSet<User>();
	
	@OneToMany(mappedBy="team")
	@JsonIgnore
	private Set<Project> projects = new HashSet<Project>();

	public Team() {
		super();
	}

	

	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}

	@JsonIgnore
	public Set<User> getUsers() {
		return users;
	}


	@JsonGetter("users")
    public List<HashMap<String,Object>> getTheUsers() {
		List<HashMap<String, Object>> liste=new ArrayList<HashMap<String,Object>>();
		
		for(User user : users){
			HashMap<String, Object> tmp = new HashMap<String, Object>();
			tmp.put("id", user.getId());
			tmp.put("username", user.getUsername());
			liste.add(tmp);
		}
		return liste;
    }

	@JsonProperty
	public void setUsers(Set<User> users) {
		this.users = users;
	}

	@JsonGetter("projects")
    public List<HashMap<String,Object>> getTheProjects() {
		List<HashMap<String, Object>> liste=new ArrayList<HashMap<String,Object>>();
		
		for(Project project : projects){
			HashMap<String, Object> tmp = new HashMap<String, Object>();
			tmp.put("id", project.getId());
			liste.add(tmp);
		}
		return liste;
    }
	
	@JsonIgnore
	public Set<Project> getProjects() {
		return projects;
	}
	
	@JsonProperty
	public void setProjects(Set<Project> projects) {
		this.projects = projects;
	}



	@Override
	public String toString() {
		return "Team [id=" + id + ", nombre de users=" + users.size() + ", nombre de projets = " + projects.size() + "]";
	}


	
	
	
	
}

package sample.undertow.domain;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity(name="version")
public class Version {
	
	@Id
	@GeneratedValue
	private long id;
	
	private String name;
	
	@Column(name="date_livraison")
	private Date dateLivraison;
	
	@ManyToOne
	private Project project;
	
	@OneToMany(mappedBy="version")
	private Collection<AbstractStory> stories = new ArrayList<AbstractStory>();

	public Version(String name, Date dateLivraison, Project project) {
		super();
		this.name = name;
		this.dateLivraison = dateLivraison;
		this.setProject(project);
	}

	public Version() {
		super();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getDateLivraison() {
		return dateLivraison;
	}

	public void setDateLivraison(Date dateLivraison) {
		this.dateLivraison = dateLivraison;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public Collection<AbstractStory> getStories() {
		return stories;
	}

	public void setStories(Collection<AbstractStory> stories) {
		this.stories = stories;
	}

	@Override
	public String toString() {
		String str = "Version [id=" + id + ", name=" + name + ", dateLivraison="
				+ dateLivraison +", stories="+ this.getStories().size()+" story";
		
		return str+"]\n"; 
	}
	
	
	
}

package sample.undertow.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;


@Entity(name="user")
public class User implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	private long id;
	
	private String username;
	
	@ManyToMany(cascade = CascadeType.MERGE)
	@JoinTable(name="users_teams",
			joinColumns = {@JoinColumn(name = "user_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "team_id", referencedColumnName = "id")}
	)
	private Set<Team> teams = new HashSet<Team>();
	
	@ManyToMany(cascade = CascadeType.MERGE)
	@JoinTable(name="users_tasks",
	joinColumns = {@JoinColumn(name = "user_id", referencedColumnName = "id")},
    inverseJoinColumns = {@JoinColumn(name = "task_id", referencedColumnName = "id")}
	)
	private Set<Task> tasks =new HashSet<Task>();

	public User(String username) {
		super();
		this.username = username;
	}

	public User() {
		super();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@JsonIgnore
	public Set<Team> getTeams() {
		return teams;
	}

	@JsonGetter("teams")
    public List<HashMap<String,Long>> getTheTeams() {
		List<HashMap<String, Long>> liste=new ArrayList<HashMap<String,Long>>();
		
		for(Team team : teams){
			HashMap<String, Long> tmp = new HashMap<String, Long>();
			tmp.put("id", team.getId());
			liste.add(tmp);
		}
		return liste;
    }
	
	@JsonProperty
	public void setTeams(Set<Team> teams) {
		this.teams = teams;
	}
	
	@JsonIgnore
	public Set<Task> getTasks() {
		return tasks;
	}
	
	@JsonGetter("tasks")
    public List<HashMap<String,Object>> getTheTasks() {
		List<HashMap<String, Object>> liste=new ArrayList<HashMap<String,Object>>();
		
		for(Task task : tasks){
			HashMap<String, Object> tmp = new HashMap<String, Object>();
			tmp.put("id", task.getId());
			liste.add(tmp);
		}
		return liste;
    }
	
	@JsonProperty
	public void setTasks(Set<Task> tasks) {
		this.tasks = tasks;
	}
	

	@Override
	public String toString() {
		return "User [id=" + id + ", username=" + username + ", nombre de team="+teams.size()+", nombre de tache="+tasks.size()+  "]";
	}
	
	
	
}

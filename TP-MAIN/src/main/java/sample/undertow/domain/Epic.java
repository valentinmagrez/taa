package sample.undertow.domain;

import java.util.ArrayList;
import java.util.Collection;
import sample.undertow.domain.Priority;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

@Entity(name="epic")
public class Epic extends AbstractStory{
	
	@OneToMany(mappedBy="epic")
	private Collection<Story> stories = new ArrayList<Story>();

	public Epic(int estimation, Priority priority, String description, Version version) {
		super(estimation, priority, description, version);
	}

	public Epic() {
		super();
	}
	
	public Collection<Story> getStories() {
		return stories;
	}

	public void setStories(Collection<Story> stories) {
		this.stories = stories;
	}

	@Override
	public String toString() {
		String str = super.toString();
		str += " Epic [stories= ";
		
		if(!stories.isEmpty()){
			for(Story story : stories){
				str+= story + "\n";
			}
		}
		
		return str;
	}
	
	

}

package sample.undertow.domain;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity(name="task")
public class Task {
	
	@Id
	@GeneratedValue
	private Long id;
	
	@ManyToMany
	@JoinTable(name="users_tasks",
	joinColumns = {@JoinColumn(name = "task_id", referencedColumnName = "id")},
	inverseJoinColumns = {@JoinColumn(name = "user_id", referencedColumnName = "id")}
	)
	@JsonIgnore
	private Collection<User> users = new ArrayList<User>();
	
	@ManyToOne
	private Story story;
	
	private String description;
	
	private boolean done;

	public Task(Story story, String description, boolean done) {
		super();
		this.story = story;
		this.description = description;
		this.done = done;
	}

	public Task() {
		super();
	}

	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}

	public Collection<User> getUsers() {
		return users;
	}

	public void setUsers(Collection<User> users) {
		this.users = users;
	}
	

	public Story getStory() {
		return story;
	}

	public void setStory(Story story) {
		this.story = story;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isDone() {
		return done;
	}

	public void setDone(boolean done) {
		this.done = done;
	}

	
	
	@Override
	public String toString() {
		return "Task [id=" + id + ", users=" + users + ", story=" + story + ", description=" + description + ", done="
				+ done + "]";
	}
}

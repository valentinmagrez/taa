package sample.undertow.domain;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;


@Entity(name="project")
public class Project {

		@Id
		@GeneratedValue
		private long id;
		
		private String name;
		
		@OneToMany(mappedBy="project")
		private List<Version> versions = new ArrayList<Version>();
		
		@ManyToOne
		private Team team;

		public Project(String name, Team team) {
			super();
			this.name = name;
			this.team=team;
		}

		public Project() {
			super();
		}

		public long getId() {
			return id;
		}

		public void setId(long id) {
			this.id = id;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public List<Version> getVersions() {
			return versions;
		}

		public void setVersions(List<Version> versions) {
			this.versions = versions;
		}
		
		@JsonIgnore
		public Team getTeam() {
			return team;
		}

		@JsonGetter("team")
	    public HashMap<String,Long> getTheTeam() {
			HashMap<String, Long> liste=new HashMap<String,Long>();
			if(team!=null)liste.put("id", team.getId());
			return liste;
	    }
		
		@JsonProperty
		public void setTeam(Team team) {
			this.team = team;
		}

		@Override
		public String toString() {
			String str="Project [id=" + id + ", name=" + name + ", team= "+ team + ", version=";
			
			if(versions != null){
				for(Version version: versions){
					str+="\n "+version.toString();
				}
			}
			
			return str+"]";
		}
		
		

}

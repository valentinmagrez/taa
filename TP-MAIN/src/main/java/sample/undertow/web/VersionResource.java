package sample.undertow.web;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import sample.undertow.domain.Version;
import sample.undertow.service.VersionService;

@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping("/api/versions")
public class VersionResource {
	@Autowired
    private VersionService versionService;

    @RequestMapping(method=RequestMethod.GET,produces=MediaType.APPLICATION_JSON_VALUE)
    public List<Version> getAll() {
        return versionService.findAll();
    }
    
    @RequestMapping(method=RequestMethod.GET, value="{id}", produces=MediaType.APPLICATION_JSON_VALUE)
    public Version getById(@PathVariable long id) {
        Version u =  versionService.findById(id);
        return u;
    }
    
    @RequestMapping(method=RequestMethod.PUT, value="{id}")
    public void updateVersion(@PathVariable long id, @RequestBody Version u) {
    	Version v = versionService.findById(id);
    	v.setDateLivraison(u.getDateLivraison());
    	v.setName(u.getName());
    	v.setProject(u.getProject());
    	v.setStories(u.getStories());
    	versionService.update(v);
    }
    
    @RequestMapping(method=RequestMethod.POST)
    public void createVersion(@RequestBody Version u){
    	versionService.create(u);
    }
    
    @RequestMapping(method=RequestMethod.DELETE, value="{id}")
    public void deleteVersion(@PathVariable long id){
    	versionService.delete(id);
    }
    
    public void setVersionRepository(VersionService u){
    	this.versionService = u;
    }
}

package sample.undertow.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import sample.undertow.domain.Project;
import sample.undertow.domain.User;
import sample.undertow.repository.ProjectRepository;
import sample.undertow.service.ProjectService;
import sample.undertow.service.UserService;

@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping("/api/projects")
public class ProjectResource {
	
	@Autowired ProjectService projectService;

    @RequestMapping(method=RequestMethod.GET,produces=MediaType.APPLICATION_JSON_VALUE)
    public List<Project> getAll() {
    	return projectService.findAll();
    }
    
    @RequestMapping(method=RequestMethod.GET, value="{id}", produces=MediaType.APPLICATION_JSON_VALUE)
    public Project getById(@PathVariable long id){
    	Project p = projectService.findById(id);
    	return p;
    }
    
    @RequestMapping(method=RequestMethod.PUT, value="{id}")
    public void updateProject(@PathVariable long id, @RequestBody Project p) {
    	Project newProject = projectService.findById(id);
    	newProject.setName(p.getName());
    	if(p.getTeam()!=null){
    		newProject.setTeam(p.getTeam());
    	}else{
    		newProject.setTeam(null);
    	}
    	newProject.setVersions(p.getVersions());
        projectService.update(newProject);
    }
    
    
    @RequestMapping(method=RequestMethod.POST)
    public void createProject(@RequestBody Project p){
    	projectService.create(p);
    }
    
    @RequestMapping(method=RequestMethod.DELETE, value="{project_id}")
    public void deleteProject(@PathVariable long project_id){
    	projectService.delete(project_id);
    }
    
    public void setProjectRepository(ProjectService p){
    	this.projectService = p;
    }
}

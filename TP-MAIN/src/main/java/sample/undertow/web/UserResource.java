package sample.undertow.web;

import java.util.List;

import org.eclipse.jetty.servlets.CrossOriginFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.CrossOrigin;

import sample.undertow.domain.User;
import sample.undertow.service.UserService;

@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping("/api/users")
public class UserResource {
	
	@Autowired
    private UserService userService;

	
    @RequestMapping(method=RequestMethod.GET,produces=MediaType.APPLICATION_JSON_VALUE)
    public List<User> getAll() {
        return userService.findAll();
    }
    
    @RequestMapping(method=RequestMethod.GET, value="{id}", produces=MediaType.APPLICATION_JSON_VALUE)
    public User getById(@PathVariable long id) {
        User u =  userService.findById(id);
        return u;
    }
    
    @RequestMapping(method=RequestMethod.PUT, value="{id}")
    public void updateUser(@PathVariable long id, @RequestBody User u) {
    	User newUser = userService.findById(id);
    	newUser.setTasks(u.getTasks());
    	newUser.setTeams(u.getTeams());
    	newUser.setUsername(u.getUsername());
    	userService.update(newUser);
    }
    
    @RequestMapping(method=RequestMethod.POST)
    public void createUser(@RequestBody User u){
    	userService.create(u);
    }
    
    @RequestMapping(method=RequestMethod.DELETE, value="{user_id}")
    public void deleteUser(@PathVariable long user_id){
    	userService.delete(user_id);
    }
    
    public void setUserRepository(UserService u){
    	this.userService = u;
    }
}

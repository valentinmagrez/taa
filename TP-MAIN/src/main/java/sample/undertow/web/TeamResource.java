package sample.undertow.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import sample.undertow.domain.Team;
import sample.undertow.service.TeamService;

@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping("/api/teams")
public class TeamResource {
	@Autowired
    private TeamService teamService;

    @RequestMapping(method=RequestMethod.GET,produces=MediaType.APPLICATION_JSON_VALUE)
    public List<Team> getAll() {
        return teamService.findAll();
    }
    
    @RequestMapping(method=RequestMethod.GET, value="{id}", produces=MediaType.APPLICATION_JSON_VALUE)
    public Team getById(@PathVariable long id) {
        Team t =  teamService.findById(id);
        return t;
    }
    
    @RequestMapping(method=RequestMethod.PUT, value="{id}")
    public void updateTeam(@PathVariable long id, @RequestBody Team t) {
    	Team newTeam = teamService.findById(id);
    	newTeam.setProjects(t.getProjects());
    	newTeam.setUsers(t.getUsers());
    	teamService.update(newTeam);
    }
    
    @RequestMapping(method=RequestMethod.POST)
    public void createTeam(@RequestBody Team t){
    	teamService.create(t);
    }
    
    @RequestMapping(method=RequestMethod.DELETE, value="{id}")
    public void deleteTeam(@PathVariable long id){
    	teamService.delete(id);
    }
    
    public void setTserRepository(TeamService t){
    	this.teamService = t;
    }
}

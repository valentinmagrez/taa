angular.module('webappApp')
  .service("restServiceUsers", ["$http", "$q",
          function($http, $q){
        	  this.getUsers = function(){
        		  var deferredUsers = $q.defer();
        		  $http({
        			  method: 'GET',
        			  url: 'http://localhost:9000/api/users'
        			}).success(function (response) {
        			    // this callback will be called asynchronously
        			    // when the response is available
        				deferredUsers.resolve(response);
        			  }).error( function (response) {
        			    // called asynchronously if an error occurs
        			    // or server returns response with an error status.
        				  alert("error");
        			  });
        		  return deferredUsers.promise;
        	  };
        	  this.postUser = function(name, teamId){
        		  var deferredUsers = $q.defer();
        		  $http({
        			  method: 'POST',
        			  url: 'http://localhost:9000/api/users',
        			  data: {"username": name, "teams": teamId}
        			}).success(function (response) {
        			    // this callback will be called asynchronously
        			    // when the response is available
        				deferredUsers.resolve(response);
        			  }).error( function (response) {
        			    // called asynchronously if an error occurs
        			    // or server returns response with an error status.
        				  alert("error");
        			  });
        		  return deferredUsers.promise;
        	  };
        	  this.deleteUser = function(id){
        		  var deferredUsers = $q.defer();
        		  $http({
        			  method: 'DELETE',
        			  url: 'http://localhost:9000/api/users/' + id,
        			}).success(function (response) {
        			    // this callback will be called asynchronously
        			    // when the response is available
        				deferredUsers.resolve(response);
        			  }).error( function (response) {
        			    // called asynchronously if an error occurs
        			    // or server returns response with an error status.
        				  alert("error");
        			  });
        		  return deferredUsers.promise;
        	  };
        	  this.updateUser = function(id, name, teams, tasks){
        		  var deferredUsers = $q.defer();
        		  $http({
        			  method: 'PUT',
        			  url: 'http://localhost:9000/api/users/'+id,
        			  data: {"username": name, "teams": teams, "tasks": tasks}
        			}).success(function (response) {
        			    // this callback will be called asynchronously
        			    // when the response is available
        				deferredUsers.resolve(response);
        			  }).error( function (response) {
        			    // called asynchronously if an error occurs
        			    // or server returns response with an error status.
        				  alert("error");
        			  });
        		  return deferredUsers.promise;
        	  };
}]);




angular.module('webappApp')
.service("restServiceTeams", ["$http", "$q",
        function($http, $q){
			this.getTeams = function(){
				  var deferredTeams = $q.defer();
				  $http({
					  method: 'GET',
					  url: 'http://localhost:9000/api/teams'
					}).success(function (response) {
					    // this callback will be called asynchronously
					    // when the response is available
						deferredTeams.resolve(response);
					  }).error( function (response) {
					    // called asynchronously if an error occurs
					    // or server returns response with an error status.
						  alert("error");
					  });
				  return deferredTeams.promise;
			};
			this.postTeam = function(id){
				  var deferredTeams = $q.defer();
				  $http({
					  method: 'POST',
					  url: 'http://localhost:9000/api/teams',
					  data: {"id": id}
					}).success(function (response) {
					    // this callback will be called asynchronously
					    // when the response is available
						deferredTeams.resolve(response);
					  }).error( function (response) {
					    // called asynchronously if an error occurs
					    // or server returns response with an error status.
						  alert("error");
					  });
				  return deferredTeams.promise;
			};
			this.deleteTeam = function(id){
				var deferredTeams = $q.defer();
	      		  $http({
	      			  method: 'DELETE',
	      			  url: 'http://localhost:9000/api/teams/' + id,
	      			}).success(function (response) {
	      			    // this callback will be called asynchronously
	      			    // when the response is available
	      				deferredTeams.resolve(response);
	      			  }).error( function (response) {
	      			    // called asynchronously if an error occurs
	      			    // or server returns response with an error status.
	      				  alert("error");
	      			  });
	      		return deferredTeams.promise;
      	  };
      	this.updateTeam = function(id, users, projects){
			  var deferredTeams = $q.defer();
			  $http({
				  method: 'PUT',
				  url: 'http://localhost:9000/api/teams/'+id,
				  data: {"id": id, "users": users, "projects": projects}
				}).success(function (response) {
				    // this callback will be called asynchronously
				    // when the response is available
					deferredTeams.resolve(response);
				  }).error( function (response) {
				    // called asynchronously if an error occurs
				    // or server returns response with an error status.
					  alert("error");
				  });
			  return deferredTeams.promise;
		};
}]);




angular.module('webappApp')
.service("restServiceProjects", ["$http", "$q",
        function($http, $q){
			this.getProjects = function(){
				  var deferredProjects = $q.defer();
				  $http({
					  method: 'GET',
					  url: 'http://localhost:9000/api/projects'
					}).success(function (response) {
					    // this callback will be called asynchronously
					    // when the response is available
						deferredProjects.resolve(response);
					  }).error( function (response) {
					    // called asynchronously if an error occurs
					    // or server returns response with an error status.
						  alert("error");
					  });
				  return deferredProjects.promise;
			};
			this.getProject = function(id){
				  var deferredProjects = $q.defer();
				  $http({
					  method: 'GET',
					  url: 'http://localhost:9000/api/projects/'+id
					}).success(function (response) {
					    // this callback will be called asynchronously
					    // when the response is available
						deferredProjects.resolve(response);
					  }).error( function (response) {
					    // called asynchronously if an error occurs
					    // or server returns response with an error status.
						  alert("error");
					  });
				  return deferredProjects.promise;
			};
			this.postProject = function(name){
				var deferredProjects = $q.defer();
				  $http({
					  method: 'POST',
					  url: 'http://localhost:9000/api/projects',
					  data: {"name": name}
					}).success(function (response) {
					    // this callback will be called asynchronously
					    // when the response is available
						deferredProjects.resolve(response);
					  }).error( function (response) {
					    // called asynchronously if an error occurs
					    // or server returns response with an error status.
						  alert("error");
					  });
				  return deferredProjects.promise;
			};
			this.deleteProject = function(id){
				var deferredProjects = $q.defer();
      		  $http({
      			  method: 'DELETE',
      			  url: 'http://localhost:9000/api/projects/' + id,
      			}).success(function (response) {
      			    // this callback will be called asynchronously
      			    // when the response is available
      				deferredProjects.resolve(response);
      			  }).error( function (response) {
      			    // called asynchronously if an error occurs
      			    // or server returns response with an error status.
      				  alert("error");
      			  });
      		  return deferredProjects.promise;
      	  };
			this.updateProject = function(id,name, team, versions){
				var deferredProjects = $q.defer();
				  $http({
					  method: 'PUT',
					  url: 'http://localhost:9000/api/projects/'+id,
					  data: {"name": name, "team": team, "versions":versions}
					}).success(function (response) {
					    // this callback will be called asynchronously
					    // when the response is available
						deferredProjects.resolve(response);
					  }).error( function (response) {
					    // called asynchronously if an error occurs
					    // or server returns response with an error status.
						  alert("error");
					  });
				  return deferredProjects.promise;
			};
}]);
'use strict';

/**
 * @ngdoc function
 * @name webappApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the webappApp
 */
angular.module('webappApp')
  .controller('MainCtrl', ["restServiceUsers", "restServiceTeams", "restServiceProjects", "$scope", "$location", 
                           function (restServiceUsers, restServiceTeams, restServiceProjects, $scope, $location){
	  
	  $scope.users = {};
	  $scope.projects = {};
	  $scope.teams = {};
	  
	  $scope.projectName = "";
	  
	  $scope.partnerName = "";
	  $scope.teamIdForUserCreation = "";
	  
	  $scope.currentObject = "";
	  
	  $scope.saveCurrentObject = function(object){
		  $scope.currentObject = object;
		  
		  //Si l'objet peut avoir plusieurs equipes
		  if($scope.currentObject.teams!=null){
			  $scope.currentObject.teamsID=new Array();
			  for(var i=0; i<$scope.currentObject.teams.length;i++){
				  console.log("ajout de l'id: "+$scope.currentObject.teams[i].id);
				  $scope.currentObject.teamsID.push($scope.currentObject.teams[i].id);
			  }
		  }
		  if($scope.currentObject.users!=null){
			  $scope.currentObject.usersID=new Array();
			  for(var i=0; i<$scope.currentObject.users.length;i++){
				  console.log("ajout de l'id: "+$scope.currentObject.users[i].id);
				  $scope.currentObject.usersID.push($scope.currentObject.users[i].id);
			  }
		  }
		  if($scope.currentObject.projects!=null){
			  $scope.currentObject.projectsID=new Array();
			  for(var i=0; i<$scope.currentObject.projects.length;i++){
				  console.log("ajout de l'id: "+$scope.currentObject.projects[i].id);
				  $scope.currentObject.projectsID.push($scope.currentObject.projects[i].id);
			  }
		  }
	  }
	  
	  $scope.getUsers = function(){
		  restServiceUsers.getUsers().then(function (response){
			  $scope.users = response;
		  });
	  };
	  
	  $scope.getTeams = function(){
		  restServiceTeams.getTeams().then(function (response){
			  $scope.teams = response;
		  });
	  }
	  
	  $scope.getProjects = function(){
		  restServiceProjects.getProjects().then(function (response){
			  $scope.projects = response;
		  });
	  }
	  
	  $scope.getTeams();
	  $scope.getUsers();
	  $scope.getProjects();
	  
	  	$scope.isActive = function (path) { 
	  		if ($location.path().substr(0, path.length) === path) {
	  		    return 'active';
	  		  } else {
	  		    return '';
	  		  }
	    };
	    
	    $scope.addPartner = function(){
	    	var tabTeam=$scope.teamIdForUserCreation;
	    	var tabForJSon=new Array();
	    	for (var i=0 ; i<tabTeam.length ; i++){
	    		tabForJSon.push({"id":tabTeam[i].id});
	    	}
	    	
	    	restServiceUsers.postUser($scope.partnerName, $scope.teamIdForUserCreation).then(function (response){
	    		$scope.getUsers();
			});
	    }
	    
	    $scope.updatePartner = function(id, name, teams){
	    	console.log("update dun utilisateur");
	    	var tabTasks=[];
	    	var tabTeam=teams;
	    	var tabForJSon=new Array();
	    	for (var i=0 ; i<tabTeam.length ; i++){
	    		tabForJSon.push({"id":tabTeam[i]});
	    	}
	    	
	    	restServiceUsers.updateUser(id, name, tabForJSon, tabTasks).then(function (response){
	    		$scope.getUsers();
			});
	    }
	    
	    $scope.removePartner = function(id){
    		restServiceUsers.deleteUser(id).then(function (response){
	    		$scope.getUsers();
			});
	    }
	    
	    $scope.addTeam = function(){
	    	restServiceTeams.postTeam($scope.teamId).then(function (response){
	    		$scope.getTeams();
			});
	    }
	    
	    $scope.removeTeam = function(id){
    		restServiceTeams.deleteTeam(id).then(function (response){
	    		$scope.getTeams();
			});
	    }
	    
	    $scope.updateTeam = function(id, users, projects){
	    	console.log("update dun utilisateur");
	    	var tabProjectsForJSon=new Array();
	    	var tabUsersForJSon=new Array();
	    	for (var i=0 ; i<users.length ; i++){
	    		tabUsersForJSon.push({"id":users[i]});
	    	}
	    	
	    	for (var i=0 ; i<projects.length ; i++){
	    		tabProjectsForJSon.push({"id":projects[i]});
	    	}
	    	
	    	console.log(tabUsersForJSon);
	    	console.log(tabProjectsForJSon)
	    	
	    	restServiceTeams.updateTeam(id, tabUsersForJSon, tabProjectsForJSon).then(function (response){
	    		$scope.getTeams();
			});
	    }
	    
	    $scope.addProject = function(){
	    	restServiceProjects.postProject($scope.projectName).then(function (response){
	    		$scope.getProjects();
			});
	    }
	    
	    $scope.removeProject = function(id){
    		restServiceProjects.deleteProject(id).then(function (response){
	    		$scope.getProjects();
			});
	    }
	    
	    $scope.updateProject = function(id,name,teamId){
	    	var team = {"id":teamId};
    		restServiceProjects.updateProject(id,name,team,null).then(function (response){
	    		$scope.getProjects();
			});
	    }

  
}]);



package taa.tp.gwt.client;


import java.util.ArrayList;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.http.client.Response;
import com.google.gwt.json.client.JSONArray;
import com.google.gwt.json.client.JSONNumber;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONParser;
import com.google.gwt.json.client.JSONString;
import com.google.gwt.json.client.JSONValue;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;


/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class webgwt implements EntryPoint {

	private ArrayList<Integer> teams = new ArrayList<Integer>();
	private ArrayList<Integer> projects = new ArrayList<Integer>();;


	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() {

		//DIfferent button
		final com.google.gwt.user.client.ui.Button buttonAddUser = new Button();
		final com.google.gwt.user.client.ui.Button buttonAddTeam = new Button();
		final com.google.gwt.user.client.ui.Button buttonAddProject = new Button();

		buttonAddUser.setText("Add user");
		buttonAddProject.setText("Add project");
		buttonAddTeam.setText("Add team");

		//data loading
		loadData(true,true,true);

		//Add button to the DOM
		RootPanel.get("buttonAddUser").add(buttonAddUser);
		RootPanel.get("buttonAddProject").add(buttonAddProject);
		RootPanel.get("buttonAddTeam").add(buttonAddTeam);
		RootPanel.get("dataSending").setVisible(false);

		buttonAddUser.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent arg0) {
				//Nom utilisateur
				final TextBox username = new TextBox();

				//Liste des equipes
				final ListBox teamSelect = new ListBox();
				teamSelect.setMultipleSelect(true);
				HTML labelTeam= new HTML("Teams : ");

				Button userValidate = new Button();
				Button userCancel = new Button();
				RootPanel inputUser = RootPanel.get("inputUser");
				RootPanel buttonUser = RootPanel.get("buttonUser");

				for(int i=0; i<teams.size();i++){
					teamSelect.addItem(teams.get(i).toString());
				}

				userValidate.setText("Ajouter");
				userCancel.setText("Annuler");

				//Affichage de la div
				if(!RootPanel.get("formUser").isVisible()){
					RootPanel.get("formUser").setVisible(true);
				}

				//Ajout des elements si c'est la premiere fois
				if(buttonUser.getWidgetCount()==0){
					//Ajout des champs et bouton
					username.getElement().setPropertyString("placeholder", "username");
					inputUser.add(username);

					inputUser.add(labelTeam);
					inputUser.add(teamSelect);

					buttonUser.add(userValidate);
					buttonUser.add(userCancel);
				}


				//Ajout des listener sur les boutons
				userValidate.addClickHandler(new ClickHandler() {

					@Override
					public void onClick(ClickEvent arg0) {
						RootPanel.get("dataSending").setVisible(true);
						ArrayList<Integer> teamsToPost=new ArrayList<Integer>();
						for(int i=0; i<teamSelect.getItemCount();i++){
							if(teamSelect.isItemSelected(i)){
								teamsToPost.add(Integer.parseInt(teamSelect.getItemText(i)));
							}
						}
						new AddService().addUser(username.getValue(), teamsToPost);
						RootPanel.get("formUser").setVisible(false);
						buttonAddUser.setVisible(true);
						loadData(true, true, false);
					}
				});
				userCancel.addClickHandler(new ClickHandler() {

					@Override
					public void onClick(ClickEvent arg0) {
						RootPanel.get("formUser").setVisible(false);
						buttonAddUser.setVisible(true);
					}
				});

				buttonAddUser.setVisible(false);
			}
		});

		buttonAddProject.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent arg0) {
				final TextBox name = new TextBox();
				HTML teamLabel = new HTML("Team: ");
				final ListBox teamSelect = new ListBox();
				Button projectValidate = new Button();
				Button projectCancel = new Button();
				RootPanel inputProject = RootPanel.get("inputProject");
				RootPanel buttonProject = RootPanel.get("buttonProject");

				projectValidate.setText("Ajouter");
				projectCancel.setText("Annuler");

				//Affichage de la div
				if(!RootPanel.get("formProject").isVisible()){
					RootPanel.get("formProject").setVisible(true);
				}

				//Ajout des elements si c'est la premiere fois
				if(buttonProject.getWidgetCount()==0){
					//Ajout des champs et bouton
					name.getElement().setPropertyString("placeholder", "name");
					inputProject.add(name);
					
					for(int i=0; i<teams.size();i++){
						teamSelect.addItem(teams.get(i).toString());
					}
					
					inputProject.add(teamLabel);
					inputProject.add(teamSelect);

					buttonProject.add(projectValidate);
					buttonProject.add(projectCancel);
				}


				//Ajout des listener sur les boutons
				projectValidate.addClickHandler(new ClickHandler() {

					@Override
					public void onClick(ClickEvent arg0) {
						RootPanel.get("dataSending").setVisible(true);
						new AddService().addProject(name.getValue(), Integer.parseInt(teamSelect.getSelectedItemText()));
						RootPanel.get("formProject").setVisible(false);
						buttonAddProject.setVisible(true);
						loadData(false, true, true);
					}
				});
				projectCancel.addClickHandler(new ClickHandler() {

					@Override
					public void onClick(ClickEvent arg0) {
						RootPanel.get("formProject").setVisible(false);
						buttonAddProject.setVisible(true);
					}
				});

				buttonAddProject.setVisible(false);
			}
		});

		buttonAddTeam.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent arg0) {
				Button teamValidate = new Button();
				Button teamCancel = new Button();
				RootPanel buttonTeam = RootPanel.get("buttonTeam");

				teamValidate.setText("Ajouter");
				teamCancel.setText("Annuler");

				//Affichage de la div
				if(!RootPanel.get("formTeam").isVisible()){
					RootPanel.get("formTeam").setVisible(true);
				}

				//Ajout des elements si c'est la premiere fois
				if(buttonTeam.getWidgetCount()==0){
					buttonTeam.add(teamValidate);
					buttonTeam.add(teamCancel);
				}


				//Ajout des listener sur les boutons
				teamValidate.addClickHandler(new ClickHandler() {

					@Override
					public void onClick(ClickEvent arg0) {
						new AddService().addTeam();
						RootPanel.get("dataSending").setVisible(true);
						RootPanel.get("formTeam").setVisible(false);
						buttonAddTeam.setVisible(true);
						loadData(false, false, true);
					}
				});
				teamCancel.addClickHandler(new ClickHandler() {

					@Override
					public void onClick(ClickEvent arg0) {
						RootPanel.get("formTeam").setVisible(false);
						buttonAddTeam.setVisible(true);
					}
				});
				buttonAddTeam.setVisible(false);
			}
		});

	}

	/**
	 * Load data
	 * @param users Si l'on souhaite recuperer les users
	 * @param users Si l'on souhaite recuperer les projects
	 * @param users Si l'on souhaite recuperer les teams
	 */
	private void loadData(boolean users, boolean projects, boolean teams){
		//Requetes GET
		if(users){
			RequestBuilder rb = new RequestBuilder(RequestBuilder.GET, "http://localhost:8080/api/users");
			rb.setCallback(new GetCallBack("users"));
	
			try {
				rb.send();
			} catch (RequestException e) {
				e.printStackTrace();
			}
		}

		if(projects){
			RequestBuilder rb = new RequestBuilder(RequestBuilder.GET, "http://localhost:8080/api/projects");
			rb.setCallback(new GetCallBack("projects"));
	
			try {
				rb.send();
			} catch (RequestException e) {
				e.printStackTrace();
			}
		}
		
		if(teams){
			RequestBuilder rb = new RequestBuilder(RequestBuilder.GET, "http://localhost:8080/api/teams");
			rb.setCallback(new GetCallBack("teams"));
	
			try {
				rb.send();
			} catch (RequestException e) {
				e.printStackTrace();
			}
		}
	}


	/**
	 * Contain all the metod to add in the db
	 * @author valentin
	 *
	 */
	class AddService{
		public void addUser(String username, ArrayList<Integer> teams){
			RequestBuilder rb = new RequestBuilder(RequestBuilder.POST, "http://localhost:8080/api/users");
			rb.setHeader("Content-Type", "application/json");
			rb.setRequestData(new CreateJsonObject().createUser(username,teams,new ArrayList<Integer>()));
			rb.setCallback(new PostRequest());
			this.sendRequest(rb);
		}

		public void addProject(String name, int team){
			RequestBuilder rb = new RequestBuilder(RequestBuilder.POST, "http://localhost:8080/api/projects");
			rb.setHeader("Content-Type", "application/json");
			rb.setRequestData(new CreateJsonObject().createProject(name,team, new ArrayList<Integer>()));
			rb.setCallback(new PostRequest());
			this.sendRequest(rb);
		}

		public void addTeam(){
			RequestBuilder rb = new RequestBuilder(RequestBuilder.POST, "http://localhost:8080/api/teams");
			rb.setHeader("Content-Type", "application/json");
			rb.setRequestData(new CreateJsonObject().createTeam());
			rb.setCallback(new PostRequest());
			this.sendRequest(rb);
		}

		private void sendRequest(RequestBuilder rb){
			try{
				rb.send();
			}catch(Exception e){

			}
		}
	}
	/**
	 * Manage get callback
	 * @author valentin
	 *
	 */
	class GetCallBack implements RequestCallback{

		private String dataMethod;

		public GetCallBack(String dataMethod) {
			this.dataMethod=dataMethod;
		}

		@Override
		public void onError(Request request, Throwable exception) {
			Window.alert(exception.getMessage());
		}

		@Override
		public void onResponseReceived(Request request,
				Response response) {
			if (200 == response.getStatusCode()) {
				GetRequest requestGet=new GetRequest();
				switch(dataMethod){
				case "users":
					requestGet.getUsers(response.getText());
					break;
				case "projects":
					requestGet.getProjects(response.getText());
					break;
				case "teams":
					requestGet.getTeams(response.getText());
					break;
				}
			}
		}
	}


	/**
	 * Create the json for different objects
	 * @author valentin
	 *
	 */
	class CreateJsonObject{

		/**
		 * Create the json string for a user
		 * @return
		 */
		public String createUser(String username, ArrayList<Integer> teamsId, ArrayList<Integer> tasksId){
			//Creation du nom d'utilisateur
			JSONObject json = new JSONObject();
			json.put("username", new JSONString(username));

			//Creation de la collection d'equipe
			JSONArray jsonArray=new JSONArray();
			JSONObject jsonTmp;

			for(int i=0; i<teamsId.size(); i++){
				jsonTmp = new JSONObject();
				jsonTmp.put("id",new JSONNumber(teamsId.get(i)));
				jsonArray.set(i, jsonTmp);
			}

			json.put("teams", jsonArray);

			//Creation de la collection d'equipe
			jsonArray=new JSONArray();

			for(int i=0; i<tasksId.size(); i++){
				jsonTmp = new JSONObject();
				jsonTmp.put("id",new JSONNumber(tasksId.get(i)));
				jsonArray.set(i, jsonTmp);
			}

			json.put("tasks", jsonArray);

			return json.toString();
		}

		/**
		 * Create the json string for a project
		 * @return
		 */
		public String createProject(String name, int teamsId, ArrayList<Integer> versionsId){
			//Creation du nom d'utilisateur
			JSONObject json = new JSONObject();
			json.put("name", new JSONString(name));

			//Ajout de l'equipe affilié
			JSONObject jsonTmp = new JSONObject();
			jsonTmp.put("id",new JSONNumber(teamsId));

			json.put("team", jsonTmp);

			//Creation de la collection d'equipe
			JSONArray jsonArray=new JSONArray();

			for(int i=0; i<versionsId.size(); i++){
				jsonTmp = new JSONObject();
				jsonTmp.put("id",new JSONNumber(versionsId.get(i)));
				jsonArray.set(i, jsonTmp);
			}

			json.put("tasks", jsonArray);

			return json.toString();
		}

		/**
		 * Create the json string for a team
		 * @return
		 */
		public String createTeam(){
			return new JSONObject().toString();
		}
	}

	/**
	 * Handle post requests
	 * @author valentin
	 *
	 */
	class PostRequest implements RequestCallback{

		@Override
		public void onError(Request arg0, Throwable arg1) {
			Window.alert("Request POST failed");
		}

		@Override
		public void onResponseReceived(Request arg0, Response arg1) {
			loadData(true, true, true);
		}

	}

	/**
	 * handle get Request
	 */
	class GetRequest{
		
		public void getUsers(String response){
			VerticalPanel vp = new VerticalPanel();
			String username="", id="", teams="";
			JSONValue jsonValue;
			JSONArray jsonArray, jsonTmpArray;
			JSONObject jsonObject, jsonTmpObject;
			JSONString jsonString;
			JSONNumber jsonNumber;
			jsonValue = JSONParser.parseStrict(response);

			if ((jsonArray = jsonValue.isArray()) == null) {
				Window.alert("Error parsing the JSON.No array.");
			}

			//Parcours la liste des items
			for(int i=0; i<jsonArray.size();i++){
				jsonValue = jsonArray.get(i);
				if ((jsonObject = jsonValue.isObject()) == null) {
					Window.alert("Error parsing the JSON. No item in array");
				}

				//Get username
				jsonValue = jsonObject.get("username");
				if ((jsonString = jsonValue.isString()) == null) {
					Window.alert("Error parsing the JSON for username");
				}else{
					username = jsonString.stringValue();
				}

				//Get ID
				jsonValue = jsonObject.get("id");
				if ((jsonNumber = jsonValue.isNumber()) == null) {
					Window.alert("Error parsing the JSON for id");
				}else{
					id=jsonNumber.toString();
				}

				//Get teams
				jsonValue = jsonObject.get("teams");
				if ((jsonTmpArray = jsonValue.isArray()) == null) {
					Window.alert("Error parsing the JSON for teams");
				}else{
					teams="{";
					for(int j=0; j<jsonTmpArray.size();j++){
						if(j>0){
							teams=teams+", ";
						}
						jsonValue = jsonTmpArray.get(j);

						if ((jsonTmpObject = jsonValue.isObject()) == null) {
							Window.alert("Error parsing the JSON. No item in teams");
						}

						jsonValue=jsonTmpObject.get("id");
						if ((jsonNumber = jsonValue.isNumber()) == null) {
							Window.alert("Error parsing the JSON for id");
						}
						teams=teams+" "+jsonNumber.toString()+" ";
					}
					teams=teams+"}";
				}

				vp.add(new HTML("<p>id: "+id+"</p><p>Username:"+username+"</p><p>"+" Teams: "+teams+"</p>"));
				RootPanel.get("listeUtilisateurs").clear();
				RootPanel.get("listeUtilisateurs").add(vp);

				RootPanel.get("dataSending").setVisible(false);
			}
		}

		private void getProjects(String response){
			VerticalPanel vp = new VerticalPanel();
			String name="", id="", team="";
			JSONValue jsonValue;
			JSONArray jsonArray;
			JSONObject jsonObject, jsonTmpObject;
			JSONString jsonString;
			JSONNumber jsonNumber;
			jsonValue = JSONParser.parseStrict(response);
			// parseStrict is available in GWT >=2.1
			// But without it, GWT is just internally calling eval()
			// which is strongly discouraged for untrusted sources



			if ((jsonArray = jsonValue.isArray()) == null) {
				Window.alert("Error parsing the JSON.No array.");
			}

			//Parcours la liste des items
			for(int i=0; i<jsonArray.size();i++){
				jsonValue = jsonArray.get(i);
				if ((jsonObject = jsonValue.isObject()) == null) {
					Window.alert("Error parsing the JSON. No item in array");
				}

				//Get username
				jsonValue = jsonObject.get("name");
				if ((jsonString = jsonValue.isString()) == null) {
					Window.alert("Error parsing the JSON for username");
				}else{
					name = jsonString.stringValue();
				}
				//						for(String test : jsonObject.keySet()){
				//							Window.alert(test);
				//						}

				//Get ID
				jsonValue = jsonObject.get("id");
				if ((jsonNumber = jsonValue.isNumber()) == null) {
					Window.alert("Error parsing the JSON for id");
				}else{
					id=jsonNumber.toString();
					projects.add(Integer.parseInt(id));
				}

				//Get teams
				jsonValue = jsonObject.get("team");
				if ((jsonTmpObject = jsonValue.isObject()) == null) {
					Window.alert("Error parsing the JSON for teams");
				}else{
					jsonValue = jsonTmpObject.get("id");
					if((jsonNumber = jsonValue.isNumber()) == null){
						Window.alert("Error parsing the JSON for project team");
					}else{
						team=jsonValue.toString();
					}
				}

				vp.add(new HTML("<p>id: "+id+"</p><p> name :"+name+"</p><p>"+" Team id: "+team+"</p>"));
				RootPanel.get("listeProjets").clear();
				RootPanel.get("listeProjets").add(vp);
				
				RootPanel.get("dataSending").setVisible(false);
			}
		}

		private void getTeams(String response){
			VerticalPanel vp = new VerticalPanel();
			String id="", projects="", users="";
			JSONValue jsonValue;
			JSONArray jsonArray, jsonTmpArray;
			JSONObject jsonObject, jsonTmpObject;
			JSONString jsonString;
			JSONNumber jsonNumber;
			jsonValue = JSONParser.parseStrict(response);
			// parseStrict is available in GWT >=2.1
			// But without it, GWT is just internally calling eval()
			// which is strongly discouraged for untrusted sources



			if ((jsonArray = jsonValue.isArray()) == null) {
				Window.alert("Error parsing the JSON.No array.");
			}

			//Parcours la liste des items
			for(int i=0; i<jsonArray.size();i++){
				jsonValue = jsonArray.get(i);
				if ((jsonObject = jsonValue.isObject()) == null) {
					Window.alert("Error parsing the JSON. No item in array");
				}

				//Get ID
				jsonValue = jsonObject.get("id");
				if ((jsonNumber = jsonValue.isNumber()) == null) {
					Window.alert("Error parsing the JSON for id");
				}else{
					id=jsonNumber.toString();
					teams.add(Integer.parseInt(id));
				}

				//Get users
				jsonValue = jsonObject.get("users");
				if ((jsonTmpArray = jsonValue.isArray()) == null) {
					Window.alert("Error parsing the JSON for teams");
				}else{
					users="{";
					for(int j=0; j<jsonTmpArray.size();j++){
						if(j>0){
							users=users+", ";
						}
						jsonValue = jsonTmpArray.get(j);

						if ((jsonTmpObject = jsonValue.isObject()) == null) {
							Window.alert("Error parsing the JSON. No item in users team");
						}

						jsonValue=jsonTmpObject.get("username");
						if ((jsonString = jsonValue.isString()) == null) {
							Window.alert("Error parsing the JSON for id");
						}
						users=users+" "+jsonString.toString()+" ";
					}
					users=users+"}";
				}

				//Get projects
				jsonValue = jsonObject.get("projects");
				if ((jsonTmpArray = jsonValue.isArray()) == null) {
					Window.alert("Error parsing the JSON for teams");
				}else{
					projects="{";
					for(int j=0; j<jsonTmpArray.size();j++){
						if(j>0){
							projects=projects+", ";
						}
						jsonValue = jsonTmpArray.get(j);

						if ((jsonTmpObject = jsonValue.isObject()) == null) {
							Window.alert("Error parsing the JSON. No item in projects team");
						}

						jsonValue=jsonTmpObject.get("id");
						if ((jsonNumber = jsonValue.isNumber()) == null) {
							Window.alert("Error parsing the JSON for id");
						}
						projects=projects+" "+jsonNumber.toString()+" ";
					}
					projects=projects+"}";
				}

				vp.add(new HTML("<p>id: "+id+"</p><p> users: "+users+"</p><p>"+" Projects: "+projects+"</p>"));
				RootPanel.get("listeEquipes").clear();
				RootPanel.get("listeEquipes").add(vp);
				
				RootPanel.get("dataSending").setVisible(false);
			}
		}
	}
}
